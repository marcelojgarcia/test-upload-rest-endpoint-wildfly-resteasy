
import java.io.File;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;
import org.junit.Test;


public class UploadResourceIT {

    @Test
    public void testUploadFile() throws Exception {

        File file = new File("src/main/resources/teste.csv");

        ResteasyClient client = new ResteasyClientBuilder().build();

        ResteasyWebTarget target = client.target("http://localhost:8080/TestUploadRestEndpoint/resources/upload/attach");

        MultipartFormDataOutput output = new MultipartFormDataOutput();
        // file (below) doesn't have to be a `byte[]`
        // It can be a `File` object and work just the same
        output.addFormData("file", file, MediaType.APPLICATION_OCTET_STREAM_TYPE);

        Response response = target.request()
                .post(Entity.entity(output, MediaType.MULTIPART_FORM_DATA));

        assertThat(response.getStatus(), is(200));
    }
}
