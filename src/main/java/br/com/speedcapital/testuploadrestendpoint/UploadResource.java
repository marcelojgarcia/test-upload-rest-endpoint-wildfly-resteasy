package br.com.speedcapital.testuploadrestendpoint;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author rcpd2013
 */
@Path("upload")
public class UploadResource {

    @GET
    public Response get() {
        return Response.ok().build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("attach")
    public Response teste2(MultipartFormDataInput multipart) {
        List<InputPart> parts = multipart.getParts();
        parts.forEach(p -> {
            MultivaluedMap<String, String> headers = p.getHeaders();
            System.out.println("headers:");
            headers.keySet().forEach(k -> {
                System.out.println("key: " + k);
                System.out.println("value: " + headers.get(k));
            });
            System.out.println("Media type: " + p.getMediaType());
        });
        return Response.ok().build();
    }

}
