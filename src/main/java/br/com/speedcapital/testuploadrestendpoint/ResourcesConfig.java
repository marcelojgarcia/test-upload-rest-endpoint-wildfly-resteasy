package br.com.speedcapital.testuploadrestendpoint;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/resources")
public class ResourcesConfig extends Application {    
}
